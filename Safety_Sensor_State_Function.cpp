/*
 * CPPFile1.cpp
 * Created: 20-6-2018 09:16:03
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Read_SSSF_Coil_Active;
bool Startup_SS_Check;

//CET set value.
int CetValue_Safety_Sensor_State_Function;

//Function Code.
void Safety_Sensor_State_Function(void)
{	
	//Read CET Analog input.
	CetValue_Safety_Sensor_State_Function = Analog_CET;
	
	//Check safety sensor state at init.
	if (Machine_Init_State == true && !Startup_SS_Check == true)
	{	
		if (digitalRead(SAFETY_SWITCH_NC) == HIGH || digitalRead(SAFETY_SWITCH_NO) == LOW)
		{
			//Alarm. Error message "Safety Sensor Error".
			SSSF_Safety_Sensor_Error = true;
			Startup_SS_Check = false;
		} 
		else
		{
			//Correct Safety Sensor State.
			SSSF_Safety_Sensor_Error = false;
			Startup_SS_Check = true;
		}
	}
	//Check safety sensor state at idle.
	if (Machine_Idle_State == true)
	{		
		if (digitalRead(SAFETY_SWITCH_NC) == HIGH && digitalRead(SAFETY_SWITCH_NO) == LOW && Safety_Sensor_Switched_State == true)
		{
			//Ensure Safety Sensor state is correct when change to Machine Running state.
		}
		else if (digitalRead(SAFETY_SWITCH_NC) == HIGH && digitalRead(SAFETY_SWITCH_NO) == LOW && !Safety_Sensor_Switched_State == true)  
		{
			//Correct Safety Sensor State after which is generated during machine running State.
			SSSF_Safety_Sensor_Error = true;
		}
		else 
		{
			//Correct Safety Sensor State.
			if (CetValue_Safety_Sensor_State_Function >= TOS)
			{
				SSSF_Safety_Sensor_Error = false;
			}
			//Reset Safety Sensor Switched state.
			Safety_Sensor_Switched_State = false;
		}
	}
	//Check safety sensor state when running.
	if (Machine_Run_State == true)
	{
		if (digitalRead(SAFETY_SWITCH_NC) == HIGH && digitalRead(SAFETY_SWITCH_NO) == LOW)
		{
			//Safety Sensor Switched State change.
			Safety_Sensor_Switched_State = true;
		}
		else
		{
			//Safety Sensor Switched State change.
			Safety_Sensor_Switched_State = false;
		}	
	}
}