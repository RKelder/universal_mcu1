/*
 * Hydraulic_Start_Function.cpp
 * Created: 18-6-2018 12:07:28
 *  Author: RK
 */

//Includes.
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Hydraulic_Start_Function(void)
{	
	//Start motor to power hydraulic pump. Contactor is NC in Manual Mode.
	if ((Hydraulic_Enable_HMI == true && MCU_Mode_Key_Switch_Input1 == false) && digitalRead(MONITORING_CONTACTOR) == HIGH && EMG_System_Active_HMI == true)
	{
		digitalWrite(CONTACTOR, HIGH);
		Hydraulic_Active = true;
	}
	else if ((Robot_Hydraulic_Enable == true && MCU_Mode_Key_Switch_Input1 == true) && digitalRead(MONITORING_CONTACTOR) == HIGH && EMG_System_Active_HMI == true)
	{
		digitalWrite(CONTACTOR, HIGH);
		Hydraulic_Active = true;		
	}
	else
	{
		digitalWrite(CONTACTOR, LOW);
		Hydraulic_Active = false;
	}		
}

