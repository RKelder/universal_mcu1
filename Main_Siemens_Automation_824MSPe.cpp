#include "Main_Siemens_Automation_824MSPe.h"
#include "Arduino.h"

//****Wire Communication
#include <Wire.h>
//Send Bytes to MCU2.
byte Send_to_MCU2_Standard_Machine_Control_0 = 0;
byte Send_to_MCU2_Standard_Machine_Control_1 = 0;
//Receive Bytes from MCU2.
byte Receive_from_MCU2_Standard_Machine_Control_0 = 0;
byte Receive_from_MCU2_Standard_Machine_Control_1 = 0;
//Wire Case.
byte Wirecase = 0;

//****Modbus Communication
#include <ModbusRtu.h>
#define ID   1
ModbusSerial<decltype(Serial)> mySerial(&Serial);
//This is device ID and RS-232 or USB-FTDI
Modbus device(ID, 0);
boolean led;
int8_t state = 0;
unsigned long tempus;
//Data array for Modbus network sharing
uint16_t au16data[133];

//****SPI Communication
#include <MCP23S17.h>
#include <SPI.h>
#include "wiring_private.h"
const uint8_t chipSelect = 10;
MCP23S17 Outputs_Y1(&SPI, ETHERNET_GTXEN, 0);
MCP23S17 Outputs_Y2(&SPI, ETHERNET_GTXEN, 1);
byte SPIcase = 0;
byte Input0;
byte Input1;
byte Input2;
byte Input3;

//Analog Read variables.
#include <ResponsiveAnalogRead.h>
ResponsiveAnalogRead CET (AI_CET, true);

//Global Variables.
bool Safety_Sensor_Error;
bool Tooling_Contact_Error;
bool Machine_Init_State;
bool Machine_Idle_State;
bool Machine_Run_State;
bool Toolingcontact_Switched_State;
bool Safety_Sensor_Switched_State;
bool Non_Conductive_Stop1;
bool Non_Conductive_Stop2;
bool TPS_Error;
bool Hyd_Cyl_Down_Release_Active;
bool Hyd_Cyl_Up_Release_Active;
int TOS;
bool SSSF_Safety_Sensor_Error;
bool EMG_Error_Reset_MCU1;
bool EMG_Insertion_machine_Active;
int Analog_CET;
int MCU1_Version_Number;

//Global Variables from or Set to MCU2.
//Wire communication. Wire not integrated. Needs to be integrated by SVB. Currently Modbus is used.
//Send to MCU2.
bool Robot_Mode_Active_MCU2;
//Send to MCU2.
bool Robot_Error_Reset_MCU2;
//Receive from MCU2.
bool Error_Set_HMI_MCU2;
//Send to MCU2.
bool EMG_Error_Reset_MCU2;
//Receive from MCU2.
bool EMG_System_Active_MCU2;
//Receive from MCU2.
bool All_At_Position_MCU2;
//Receive from MCU2.
bool Press_Complete_MCU2;
//Send to MCU2.
bool Robot_Press_Active_MCU2;
//Send to MCU2.
bool Lower_Tool_Pin_Down_Sensor_MCU2;
//Send to MCU2.
bool Lower_Tool_Pin_Up_Sensor_MCU2;
//Feed Fastener Signal from Robot to HMI.
bool Robot_Feed_Fastener_MCU2;
//Robot Shuttle Lock.
bool Robot_Shuttle_Lock_MCU2;
//Dwell Time from MCU2.
bool Dwell_Time_Active_MCU2;
//Setup Stroke send to MCU2.
bool Setup_Stroke_MCU2;
//Hydraulic Up Signal from MCU2.
bool Up_Signal_MCU2;
//Down Solenoid Block signal from MCU2.
bool Down_Solenoid_Block_MCU2;
//Robot Error Reset HMI.
bool Robot_Error_Reset_HMI;

//Modbus HMI Variables.
bool Hydraulic_Enable_HMI;
bool ConductiveM_HMI;
bool Tooling_Contact_Error_HMI;
bool Laser_Light_HMI;
bool Work_Light_HMI;
int Force_Set_Value_HMI;
bool Buzzer_HMI;
//Upper Bypass Value. Cylinder will slow down passing (above) this point.
int Bypass_Upper_Value_HMI;
//Lower Bypass Value. Cylinder will slow down passing (below) this point.
int Bypass_Lower_Value_HMI;
//Upper TPS Value.
int TPS_Calibrated_Upper_Value_HMI;
//Lower TPS Value.
int TPS_Calibrated_Lower_Value_HMI;	
//Lower Bypass Value.
int TPS_Offset_Value_HMI;
//Error Reset.
bool Error_Reset_HMI;
//EMG Error HMI.
bool EMG_System_Active_HMI;
//Top Of Stroke Position.
int TOS_Position_HMI;
//Hydraulic Active.
bool Hydraulic_Active;
//Buzzer Number Beeps HMI.
int Buzzer_Number_Beeps_HMI;
//Buzzer Beep Time HMI.
int Buzzer_Beep_Time_HMI;
//Buzzer Silent Time HMI.
int Buzzer_Silent_Time_HMI;
//Modbus Coil 4 Read.
int Read_Coil_4;
//Modbus Coil 4 Write.
int Write_Coil_4;
//Setup Stroke HMI.
bool Setup_Stroke_HMI;
//Fastener Length Calibration Value HMI.
int Fastener_Length_Calibrated_Value_HMI;
//Fastener Length HMI.
bool Fastener_Length_HMI;
//Auto Tooling HMI.
int Tooling_Mode_HMI;
//Error_Screen_Active_HMI
bool Error_Screen_Active_HMI;

//Robot Variables.
//Robot Eject Signal.
bool Robot_MAS_Eject;
//Robot Program_Byte
int Robot_Program_Byte;
//HMI Program Byte.
int Program_Byte_From_HMI;
//Program Byte from HMI.
int Robot_Program_ID_To_HMI;
//CET Output to Robot.
int Robot_CetValue_Output;
//Pressure Switch Output to Robot.
int Robot_Pressure_Switch_Output;
//Robot Lower Tool Pin Up Input.
bool Robot_Lower_Tool_Pin_Up;
//Robot Lower Tool Pin Down Input.
bool Robot_Lower_Tool_Pin_Down;
//MCU Lower Tool Pin Up movement Output.
bool MCU_Lower_Tool_Pin_Up_Output;
//MCU Lower Tool Pin Down movement Output.
bool MCU_Lower_Tool_Pin_Down_Output;
//MCU Lower Tool Pin Sensor Up Input.
bool MCU_Lower_Tool_Pin_Sensor_Up_Input;
//MCU Robot Lower Tool Pin Sensor Up Output.
bool Robot_Lower_Tool_Pin_Sensor_Up_Output;
//Robot Lower Tool Pin Sensor Down Output.
bool MCU_Lower_Tool_Pin_Sensor_Down_Input;
//Robot Lower Tool Pin Sensor Down Output.
bool Robot_Lower_Tool_Pin_Sensor_Down_Output;
//MCU Robot/Manual Key switch input at MCU1.
bool MCU_Mode_Key_Switch_Input1;
//MCU Robot/Manual Key switch input at MCU1.
bool MCU_Mode_Key_Switch_Input2;
//Robot Mode Active Output Active.
bool Robot_Mode_Active_Output1;
//Robot Mode Active Output Active.
bool Robot_Mode_Active_Output2;
//Robot Error Reset Input.
bool Robot_Error_Reset_Input;
//Robot Error Set Output (To Robot).
bool Robot_Error_Set;
//External E-Stop Robot Input #1.
bool E_Stop_EXT_1_Robot;
//External E-Stop Robot Input #2.
bool E_Stop_EXT_2_Robot;
//Robot EMG Error Reset.
bool Robot_EMG_Error_Reset;
//EMG Error output from Insertion machine to Robot.
bool Robot_EMG_Error_Set;
//All At Position Signal to Robot.
bool Robot_All_At_Position;
//Send Press Complete Signal to Robot from MCU2.
bool Robot_Press_Complete;
//Robot Press Active.
bool Robot_Press_Active_MCU1;
//Press Signal Input from Robot.
bool Robot_Press_Signal;
//Activate Hydraulic by Robot Output.
bool Robot_Hydraulic_Enabled_Input;
//Internal variable to Activate Hydraulic.
bool Robot_Hydraulic_Enable;
//Active Robot Mode.
bool Robot_Mode_Active_MCU1;
//Robot Feed Signal.
bool Robot_MAS_Feed_Signal;
//Robot Input to Lock Shuttle.
bool Robot_Shuttle_Lock;
bool Shuttle_Retract_MCU1;
//Shuttle Extend Output to Robot.
bool Shuttle_Extend_MCU1;
//Shuttle Retract Signal from MCU2.
bool Shuttle_Retract_MCU2;
//Shuttle Extend Signal from MCU2.
bool Shuttle_Extend_MCU2;
//TOS output to Robot to ensure Robot knows that Hydraulic cylinder is at TOS.
bool Hydraulic_Cylinder_TOS_Robot;

//Global Timer Variables.
bool Timer1_Q;
bool Timer1_Start;

//SPI Byte Delay.
int SPIMs = 50;
unsigned long SPIStart;

//Wire Delay Timer.
int WireMs = 10;
unsigned long WireStart;

//Analog Output Channel A.
void AnalogOutChannelA()
{
	//SPI Analog Outputs.
	//Write Analog CET signal directly to Robot Input.
	//Channel A = Pin#:10; Pressure Switch.
	//Channel B = Pin#:11; CET.
	//Write Analog Pressure Switch signal directly to Robot Input.
	int channelA = Robot_Pressure_Switch_Output | 0b0011000000000000;
	//Transfer Data.
	digitalWrite(ETHERNET_GTX1, LOW);
	SPI.transfer(highByte(channelA));
	SPI.transfer(lowByte(channelA));
	digitalWrite(ETHERNET_GTX1, HIGH);
}

//Analog Output Channel B.
void AnalogOutChannelB()
{
	//SPI Analog Outputs.
	//Write Analog CET signal directly to Robot Input.
	//Channel A = Pin#:10; Pressure Switch.
	//Channel B = Pin#:11; CET.
	//Write Analog Pressure Switch signal directly to Robot Input.
	int channelB = Robot_CetValue_Output | 0b1011000000000000;
	//Transfer Data.
	digitalWrite(ETHERNET_GTX1, LOW);
	SPI.transfer(highByte(channelB));
	SPI.transfer(lowByte(channelB));
	digitalWrite(ETHERNET_GTX1, HIGH);
}

//Digital and SPI Outputs.
void Outputs()
{
	//SPI Digital Outputs.
	//Lower Tool Pin Sensor "Up" Output to Robot.
	//Y221. Bit#:1. Pin#:46.
	if (Robot_Lower_Tool_Pin_Sensor_Up_Output == true)
	{Outputs_Y2.digitalWrite(1, HIGH);}
	else
	{Outputs_Y2.digitalWrite(1, LOW);}
	//Lower Tool Pin Sensor "Down" Output to Robot.
	//Y222. Bit#:1. Pin#:34.
	if (Robot_Lower_Tool_Pin_Sensor_Down_Output == true)
	{Outputs_Y2.digitalWrite(2, HIGH);}
	else
	{Outputs_Y2.digitalWrite(2, LOW);}
	//Robot Mode Key Switch Output #1 to Robot.
	//Y226. Bit#:6. Pin#:36.
	if (Robot_Mode_Active_Output1 == true)
	{Outputs_Y2.digitalWrite(6, HIGH);}
	else
	{Outputs_Y2.digitalWrite(6, LOW);}
	//Robot Mode Key Switch Output #2 to Robot.
	//Y227. Bit#:7. Pin#:24.
	if (Robot_Mode_Active_Output2 == true)
	{Outputs_Y2.digitalWrite(7, HIGH);}
	else
	{Outputs_Y2.digitalWrite(7, LOW);}
	//Send Insertion Machine Error State to Robot.
	//Y210. Bit#:0 Pin#:54.
	if (Robot_Error_Set == true)
	{Outputs_Y1.digitalWrite(0, HIGH);}
	else
	{Outputs_Y1.digitalWrite(0, LOW);}
	//Send Shuttle Retract Position To Robot.
	//Y216 Bit#:6 Pin#:32
	if (Shuttle_Retract_MCU1 == true)
	{Outputs_Y1.digitalWrite(6, HIGH);}
	else
	{Outputs_Y1.digitalWrite(6, LOW);}
	//Send Shuttle Extend Position To Robot.
	//Y217 Bit#:7 Pin#:20
	if (Shuttle_Extend_MCU1 == true)
	{Outputs_Y1.digitalWrite(7, HIGH);}
	else
	{Outputs_Y1.digitalWrite(7, LOW);}
	//Send EMG Error to Robot from MCU1.
	//Done by Exit:E-Stop.
	if (EMG_Insertion_machine_Active == true)
	{Outputs_Y1.digitalWrite(31, HIGH);}
	else
	{Outputs_Y1.digitalWrite(31, LOW);}
	//Send EMG Error to Robot from MCU2.
	//Done by Exit:E-Stop.
	if (EMG_Insertion_machine_Active == true)
	{Outputs_Y1.digitalWrite(32, HIGH);}
	else
	{Outputs_Y1.digitalWrite(32, LOW);}
	//Send All At Position to Robot from MCU2.
	//Y220 Bit#:0 Pin#:58
	if (Robot_All_At_Position == true)
	{Outputs_Y2.digitalWrite(0, HIGH);}
	else
	{Outputs_Y2.digitalWrite(0, LOW);}
	//Send Press_Complete to Robot from MCU2.
	//Y224 Bit#:4 Pin#:60
	if (Robot_Press_Complete == true)
	{Outputs_Y2.digitalWrite(4, HIGH);}
	else
	{Outputs_Y2.digitalWrite(4, LOW);}
	//Send TOS to Robot.
	//Y225 Bit#:5 Pin#:48
	if (Hydraulic_Cylinder_TOS_Robot == true)
	{Outputs_Y2.digitalWrite(5, HIGH);}
	else
	{Outputs_Y2.digitalWrite(5, LOW);}					
	
	//Program number send to Robot.
	//Y200-207. Pin#:50,38,26,14,52,40,28,16
	Outputs_Y1.writePort(1,Program_Byte_From_HMI);
}

//Digital and SPI Inputs.
void Inputs()
{
	//SPI Communication. Input Section of SPI Communication.
	//Read CID#0 0100_0000 = 0x40
	digitalWrite(SS, LOW);
	SPI.transfer(0x40);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	Input0 = SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	
	//Read CID#1 0100_1000 = 0x48
	digitalWrite(SS, LOW);
	SPI.transfer(0x48);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	Input1 = SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	
	//Read CID#2 0100_0100 = 0x44
	digitalWrite(SS, LOW);
	SPI.transfer(0x44);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	Input2 = SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	
	//Read CID#3 0100_0100 = 0x42; Dummy to ensure the last byte is shifted.
	digitalWrite(SS, LOW);
	SPI.transfer(0x42);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	digitalWrite(SS, LOW);
	Input3 = SPI.transfer(0x00);
	digitalWrite(SS, HIGH);
	
	//SPI Communication.
	//Program Selection by Robot. Robot Program Selection only possible when Key switch is set to Robot Mode.
	//X200-207.
	if (MCU_Mode_Key_Switch_Input1 == true)
	{
	Robot_Program_Byte												=		Input0; //SPI Input. Direct Byte from Robot.
	Robot_Program_ID_To_HMI = Robot_Program_Byte;
	}
	else
	{
	Robot_Program_ID_To_HMI = 0;
	}
	
	//Digital inputs from Automation Board.
	//Robot Mode Key Switch MCU1 Input #1 from Automation Board.
	MCU_Mode_Key_Switch_Input1						=						digitalRead(ETHERNET_GMDC)	== HIGH;
	//Robot Mode Key Switch MCU1 Input #2 from Automation Board.
	MCU_Mode_Key_Switch_Input2						=						digitalRead(ETHERNET_GMDIO) == HIGH;
	//E-Stop Ext Input#1 from External safety System from Automation Board. Pin#:3
	E_Stop_EXT_1_Robot								=						digitalRead(ETHERNET_GRX1)	== HIGH;
	//E-Stop Ext Input#2 from External safety System from Automation Board. Pin#:5
	E_Stop_EXT_2_Robot								=						digitalRead(ETHERNET_GRX0)	== HIGH;
	//Lower Tool Pin Sensor "Up" Input from Automation Board.
	MCU_Lower_Tool_Pin_Sensor_Up_Input				=						digitalRead(ETHERNET_GTXER) == HIGH;
	//Lower Tool Pin Sensor "Down" Input from Automation Board.
	MCU_Lower_Tool_Pin_Sensor_Down_Input			=						digitalRead(ETHERNET_GTXCK) == HIGH;
	
	//SPI Digital Inputs.
	//Error Reset Input from Robot.
	//X210. Bit#:8 Pin#:53.
	Robot_Error_Reset_Input							=						bitRead(Input1,0);
	//Hydraulic Enable Signal from Robot.
	//X211. Bit#:9 Pin#:41.
	Robot_Hydraulic_Enabled_Input					=						bitRead(Input1,1);
	//MAS_Feed Signal from Robot.
	//X212. Bit#:10 Pin#:29.
	Robot_MAS_Feed_Signal							=						bitRead(Input1,2);
	//MAS_Feed Signal from Robot.
	//X213. Bit#:11 Pin#:17.
	Robot_Shuttle_Lock								=						bitRead(Input1,3);
	//Lower Tool Pin "Up" Input from Robot.
	//X221. Bit#:18 Pin#:45.
	Robot_Lower_Tool_Pin_Up							=						bitRead(Input2,1);
	//Lower Tool Pin "Down" Input from Robot.
	//X222. Bit#:19 Pin#:33.
	Robot_Lower_Tool_Pin_Down						=						bitRead(Input2,2);
	//Press Signal from Robot.
	//X225. Bit#:21 Pin#:47.
	Robot_Press_Signal								=						bitRead(Input2,5);
	//EMG Error Reset from Robot.
	//X226. Bit#:22 Pin#:35.
	Robot_EMG_Error_Reset							=						bitRead(Input2,6);	
}

//Main System Setup Code (Initialization)
void setup()
{		
	//Wire Communication Start.
	Wire.begin();
	
	//Modbus Communication Setup.
	//start communication.
	//device.begin(&mySerial, 480000);
	//device.begin(&mySerial, 128000);
	device.begin(&mySerial, 9600);
	
	//SPI  Input Communication.
	pinMode (SS, OUTPUT);
	SPI.begin();
	
	//SPI Output Communication.
	Outputs_Y1.begin();
	Outputs_Y2.begin();
	
	//Analog Setup
	CET.setAnalogResolution(4096);
		
	//Machine E-Stop Input #2. E-Stop 2 needed to ensure 24Volt is continuously on.
	pinMode(ESTOP2, INPUT_PULLDOWN);
	//Initialize Contactor Pin.
	pinMode(CONTACTOR, OUTPUT);
	// Initialize Start Button.
	pinMode(START_BUTTON, INPUT_PULLDOWN);
	//Initialize Down Solenoid Pin.
	pinMode(DOWN_SOLENOID, OUTPUT);
	//Initialize Down1 Pedal.
	pinMode(FOOT_DOWN1, INPUT_PULLDOWN);
	//Initialize Down2 Pedal.
	pinMode(FOOT_DOWN2, INPUT_PULLDOWN);
	//Initialize Up Pedal.
	pinMode(FOOT_UP, INPUT_PULLDOWN);
	//Initialize Analog.
	digitalWrite(ANALOG_POWER, HIGH);
	//Set analog read resolution to 12bits (0-4095) instead of standard 10bits (0-1023).
	analogReadResolution(12);
	//Set analog write resolution to 12bits (0-4095) instead of standard 10bits (0-1023).
	analogWriteResolution(12);
	//Initialize Safety Sensors.
	pinMode(SAFETY_SWITCH_NC, INPUT_PULLDOWN);
	pinMode(SAFETY_SWITCH_NO, INPUT_PULLDOWN);
	//Initialize Tooling contact.
	pinMode(CONDUCTIVE_TC, INPUT_PULLDOWN);
	//Initialize Laser Light.
	pinMode(LASER, OUTPUT);
	//Initialize Work Light.
	pinMode(WORKLIGHT, OUTPUT);
	//Initialize Buzzer.
	pinMode(BUZZER, OUTPUT);
	//Initialize Lower Tool Pin.
	pinMode(LTC_SAFE_VALVE, OUTPUT);
	
	//Key Switch (Robot/Manual) Input #1.
	pinMode(ETHERNET_GMDC, INPUT_PULLDOWN);
	//Key Switch (Robot/Manual) Input #2.
	pinMode(ETHERNET_GMDIO, INPUT_PULLDOWN);
	//E_STOP Input from Robot. #2.
	pinMode(ETHERNET_GRX0, INPUT_PULLDOWN);
	//E_STOP Input from Robot. #1.
	pinMode(ETHERNET_GRX1, INPUT_PULLDOWN);
	//Lower Tool Pin Retract.
	pinMode(ETHERNET_GTXCK, INPUT_PULLDOWN);
	//Lower Tool Pin Extend.
	pinMode(ETHERNET_GTXER, INPUT_PULLDOWN);
	//Analog Input Reading (CET and Pressure Switch).
	pinMode(ETHERNET_GTX1, OUTPUT);
	
	//Setup Buzzer Function.
	void Setup_Buzzer_Function();
	//Setup Hydraulic Cylinder Function.
	void Setup_Hydraulic_Cylinder_Function();
	
	//SPI Pin Setup Digital Inputs/Outputs.
	Outputs_Y1.pinMode(0, OUTPUT);
	Outputs_Y1.pinMode(1, OUTPUT);
	Outputs_Y1.pinMode(2, OUTPUT);
	Outputs_Y1.pinMode(3, OUTPUT);
	Outputs_Y1.pinMode(4, OUTPUT);
	Outputs_Y1.pinMode(5, OUTPUT);
	Outputs_Y1.pinMode(6, OUTPUT);
	Outputs_Y1.pinMode(7, OUTPUT);
	
	Outputs_Y1.pinMode(8, OUTPUT);
	Outputs_Y1.pinMode(9, OUTPUT);
	Outputs_Y1.pinMode(10, OUTPUT);
	Outputs_Y1.pinMode(11, OUTPUT);
	Outputs_Y1.pinMode(12, OUTPUT);
	Outputs_Y1.pinMode(13, OUTPUT);
	Outputs_Y1.pinMode(14, OUTPUT);
	Outputs_Y1.pinMode(15, OUTPUT);
	
	Outputs_Y2.pinMode(0, OUTPUT);
	Outputs_Y2.pinMode(1, OUTPUT);
	Outputs_Y2.pinMode(2, OUTPUT);
	Outputs_Y2.pinMode(3, OUTPUT);
	Outputs_Y2.pinMode(4, OUTPUT);
	Outputs_Y2.pinMode(5, OUTPUT);
	Outputs_Y2.pinMode(6, OUTPUT);
	Outputs_Y2.pinMode(7, OUTPUT);
}

//Wire Communication. Receive Bytes from MCU2.
void Wire_Communication_Receive()
{
	//Request 2 Bytes from device #2 (MCU2).
	Wire.requestFrom(4,2);
	//While Loop.
	while(Wire.available())
	{
		Receive_from_MCU2_Standard_Machine_Control_0 = Wire.read();
		Receive_from_MCU2_Standard_Machine_Control_1 = Wire.read();
	}
	//Read bit 0 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Error_Set_HMI_MCU2			= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 0);
	//Read bit 1 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	EMG_System_Active_MCU2		= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 1);
	//Read bit 2 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	All_At_Position_MCU2		= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 2);
	//Read bit 3 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Press_Complete_MCU2			= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 3);
	//Read bit 4 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Dwell_Time_Active_MCU2		= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 4);
	//Read bit 5 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Setup_Stroke_MCU2			= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 5);
	//Read bit 6 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Shuttle_Extend_MCU2			= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 6);
	//Read bit 7 from Byte 0.Receive_from_MCU2_Standard_Machine_Control_0.
	Shuttle_Retract_MCU2		= bitRead(Receive_from_MCU2_Standard_Machine_Control_0, 7);
	
	//Read bit 0 from Byte 1.Receive_from_MCU1_Standard_Machine_Control_1.
	Up_Signal_MCU2				= bitRead(Receive_from_MCU2_Standard_Machine_Control_1, 0);
	//Read bit 1 from Byte 1.Receive_from_MCU1_Standard_Machine_Control_1.
	Down_Solenoid_Block_MCU2	= bitRead(Receive_from_MCU2_Standard_Machine_Control_1, 1);		
}

//Wire Communication. Send Bytes to MCU2.
void Wire_Communication_Send()
{
	//Send Byte #0. "Send_to_MCU2_Standard_Machine_Control_0".
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,0,Robot_Mode_Active_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,1,Robot_Error_Reset_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,2,EMG_Error_Reset_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,3,Robot_Press_Active_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,4,MCU_Lower_Tool_Pin_Up_Output);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,5,MCU_Lower_Tool_Pin_Down_Output);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,6,Lower_Tool_Pin_Up_Sensor_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_0,7,Lower_Tool_Pin_Down_Sensor_MCU2);
	//Send Byte #1. "Send_to_MCU2_Standard_Machine_Control_1".
	bitWrite(Send_to_MCU2_Standard_Machine_Control_1,0,Robot_Feed_Fastener_MCU2);
	bitWrite(Send_to_MCU2_Standard_Machine_Control_1,1,Robot_Shuttle_Lock_MCU2);
	
	//Transmit to device #2 (MCU2).
	Wire.beginTransmission(4);
	//Send Byte 0 and 1.
	Wire.write(Send_to_MCU2_Standard_Machine_Control_0);
	Wire.write(Send_to_MCU2_Standard_Machine_Control_1);              
	//Stop transmitting.
	Wire.endTransmission();
}

//Main program code into the continuous loop() function.
void loop()
{				
	//Modbus Communication Loop.
	//Poll messages
	//Blink led pin on each valid message
	state = device.poll( au16data, 133);
	if (state > 4)
	{tempus = millis() + 50; digitalWrite(13, HIGH);}
	if (millis() > tempus) digitalWrite(13, LOW );
	Io_Poll();
	
	//Analog read.
	CET.update();
	Analog_CET = CET.getValue();
	
	//Function blocks.
	Hydraulic_Start_Function();
	Footpedal_Function();
	Bypass_Function();
	Force_Function();
	Safety_Sensor_State_Function();
	Toolingcontact_Function();
	Conductive_Mode_Function();
	Machine_State_Function();
	Initialization_Function();
	Timer_Function();
	Alarm_Function();
	Non_Conductive_Mode_Function();
	Laser_Light_Function();
	Work_Light_Function();
	Buzzer_Function();
	Tooling_Protection_System_Function();
	Hydraulic_Cylinder_Function();
	Safety_Block_Function();
	TOS_Function();
	Robot_Communication_Function();
	Robot_Lower_Tool_Pin_Function();
	MCU_Version_Function();
	
	//SPI Byte Communication.
	if ((millis() - SPIMs) >= SPIStart) 
	{
		SPIStart = millis();
		switch(SPIcase) 
		{
			case 0:
			//SPI for the outputs. 1MHz.
			//SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
			//SPI for the outputs. 0.1MHz (100KHz).
			SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE0));
			Outputs();
			SPI.endTransaction();
			SPIcase = 1;
			break;
			case 1:
			//SPI for the inputs. 1MHz.
			//SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
			//SPI for the inputs. 1MHz.0.1MHz (100KHz).
			SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE3));
			Inputs();
			SPI.endTransaction();
			SPIcase = 2;
			break;
			case 2:
			//SPI for analog output channel A. 1Mhz.
			//SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
			//SPI for analog output channel A. 1Mhz. 1MHz.0.1MHz (100KHz)
			SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE0));
			AnalogOutChannelA();
			SPI.endTransaction();
			SPIcase = 3;
			break;
			case 3:
			//SPI for analog output channel B. 1MHz.
			//SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
			//SPI for analog output channel B. 1MHz. 1MHz.0.1MHz (100KHz)
			SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE0));
			AnalogOutChannelB();
			SPI.endTransaction();
			SPIcase = 0;
			break;
		}
	}
	
	//Wire Communication. Send.
	Wire_Communication_Send();
	//Wire Delay Case.
	if ((millis() - WireMs) >= WireStart)
	{
		WireStart = millis();
		switch(Wirecase)
		{
			//Case 0.
			case 0:
			Wire_Communication_Receive();
			Wirecase = 1;
			break;
			//Case 1.
			case 1:
			//Dummy.
			Wirecase = 0;
			break;
		}
	} 
}

//Modbus communication between Master and Slave (PCB board = Slave).
void Io_Poll()
{
	//Read digital outputs/inputs from HMI -> au16data[0].	
	Hydraulic_Enable_HMI						=		bitRead( au16data[0],0);		//0**1
	ConductiveM_HMI								=		bitRead( au16data[0],1);		//1
	Laser_Light_HMI								=		bitRead( au16data[0],2);		//2**1
	Work_Light_HMI								=		bitRead( au16data[0],3);		//3
	if (Read_Coil_4 == true)
	{Buzzer_HMI									= 		bitRead( au16data[0],4);}		//4
	Setup_Stroke_HMI							=		bitRead( au16data[0],7);		//7
	Fastener_Length_HMI							=		bitRead( au16data[0],8);		//8
	Error_Screen_Active_HMI						=		bitRead( au16data[0],9);		//9
	
	//Write digital outputs/inputs to HMI -> au16data[0].
	if (Write_Coil_4 == true)
	{bitWrite (au16data[0],4, Buzzer_HMI);}												//4
	bitWrite (au16data[0],5, EMG_System_Active_HMI = 1);								//5
	bitWrite (au16data[0],6, Hydraulic_Active);											//6
	
	//Read digital Error outputs/inputs from HMI -> au16data[1].
	Error_Reset_HMI								=		bitRead( au16data[1],0);		//16
	
	//Write digital outputs to MCU1 -> au16data[6].
	bitWrite (au16data [6],0,Robot_Error_Reset_HMI);									//96
	
	//Read register values from HMI-> au16data[10].
	Bypass_Lower_Value_HMI						=		(au16data[10]);					//Register:10**2000
	Bypass_Upper_Value_HMI						=		(au16data[11]);					//Register:11**3400
	Force_Set_Value_HMI							=		(au16data[12]);					//Register:12**1000						
	TPS_Offset_Value_HMI						=		(au16data[13]);					//Register:13**1000
	TPS_Calibrated_Upper_Value_HMI				=		(au16data[14]);					//Register:14
	TPS_Calibrated_Lower_Value_HMI				=		(au16data[15]);					//Register:15
	TOS_Position_HMI							=		(au16data[16]);					//Register:16**3600
	Buzzer_Number_Beeps_HMI						=		(au16data[17]);					//Register:17
	Buzzer_Beep_Time_HMI						=		(au16data[18]);					//Register:18
	Buzzer_Silent_Time_HMI						=		(au16data[19]);					//Register:19
	Fastener_Length_Calibrated_Value_HMI		=		(au16data[20]);					//Register:20
	Tooling_Mode_HMI							=		(au16data[21]);					//Register:21
	
	//Write values directly from coil to HMI -> au16data[30].
	au16data[30]								=		analogRead(AI_PRESSURE_SWITCH);	//Register:30
	au16data[31]								=		analogRead(AI_CET);				//Register:31
	
	//Send Byte (with Program number) from Robot to HMI [100]. X200-X207.
	(au16data[130])								=		Robot_Program_ID_To_HMI;		//Register:120
	
	//Receive one Byte from HMI to select program at Robot # (Robot extension board) [101]. Y200-Y207
	Program_Byte_From_HMI						=		(au16data[131]);				//Register:131
	
	//Send MCU1 version number to HMI [132]
	(au16data[132])								=		MCU1_Version_Number;			//Register:132

	//Diagnose communication -> au16data[40].
	au16data[40] = device.getInCnt();
	au16data[41] = device.getOutCnt();
	au16data[42] = device.getErrCnt();
}