/*
 * Work_Light_Function.cpp
 * Created: 27-6-2018 09:38:34
 *  Author: RK
 */ 

//Includes.
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Work_Light_Function(void)
{
	//Laser light always on.
	Work_Light_HMI = 1;
	
	if (Work_Light_HMI == true)
	{
		digitalWrite(WORKLIGHT, HIGH);
	}
	else
	{
		digitalWrite(WORKLIGHT, LOW);
	}
}