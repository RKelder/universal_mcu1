/*
 * Laser_Light_Function.cpp
 * Created: 27-6-2018 09:37:52
 *  Author: RK
 */ 

//Includes.
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Laser_Light_Function(void)
{
	//Laser light always on.
	Laser_Light_HMI = 1;
	
	if (Laser_Light_HMI == true)
	{
		digitalWrite(LASER, HIGH);
	}
	else
	{
		digitalWrite(LASER, LOW);
	}
}